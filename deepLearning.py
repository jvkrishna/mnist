'''
Created on Feb 28, 2017

@author: krishna
'''
import numpy as np;
from keras.models import Sequential;
from keras.layers import Dense, Dropout;
from keras.callbacks import EarlyStopping;
from keras.utils import np_utils; 
from sklearn.metrics import confusion_matrix;
import matplotlib.pyplot as plot;
from matplotlib.pyplot import show;
from sklearn.metrics.classification import classification_report
from keras.callbacks import  CSVLogger
from keras.optimizers import rmsprop


# Parameters [Adjust these values for experimentation]
train_file_path = "data/optdigits.tra";
test_file_path = "data/optdigits.tes";
nb_hidden_layers = 1;
# provide number of hidden units in each layer. Size of this array should match with the number of layers.
nb_hidden_units = np.array([8]);
activation = "sigmoid";  # sigmoid, tanh, relu
loss = "mean_squared_error";  # categorical_crossentropy, mse
dropout = 0.5;
learning_rate = 0.1;
optimizer = rmsprop(lr=learning_rate);
metrics = ['accuracy'];
validation_split = 0.2;
early_stopping = EarlyStopping(monitor='val_loss', min_delta=1e-4); 
nb_epoch = 10;
verbose = True;


def printParams():
    report = "Model Parameters, \n";
    report += "Number of Hidden Layers : %d\n" % nb_hidden_layers;
    report += "Hidden Units in each layer : %s\n" % nb_hidden_units;
    report += "Activation Function: %s\n" % activation;
    report += "Loss Function: %s\n" % loss;
    report += "Optimizer Function: %s\n" % optimizer;
    report += "Learning Rate: %s\n" %learning_rate;
    report += "Dropout: %f\n" %dropout;
    report += "Number of Epochs: %d\n" % nb_epoch;
    report += "Validation Split: %.2f\n" % validation_split;
    report += "Early Stopping, Minimum Delta: %f\n" % early_stopping.min_delta;
    print(report, end="\n\n");


def printMetrics(y_true, y_pred, score):
    # Print accuracy
    for i, metric in enumerate(metrics):
        print("%s: %.2f%%" % (metric, score[i + 1] * 100), end="\n\n");
        
    # print confusion matrix
    CM = confusion_matrix(y_true, y_pred);
    print("Confusion Matrix:", end="\n");
    print(CM, end=' \n\n');
    # print classification report
    target_names = ''.join(str(x) for x in range(10));
    print(classification_report(y_true, y_pred, target_names=target_names));
    # Class Accuracy
    print("Printing Class Accuracy", end="\n\n");
    for i in range(10) :
        print("Class %d accuracy is %.2f%%" % (i, CM[i, i] / sum(CM[i, :]) * 100), end='\n\n');

def imPlot(hist):
    train_loss = hist.history['loss'];
    val_loss = hist.history['val_loss'];
    train_acc = hist.history['acc'];
    val_acc = hist.history['val_acc'];
    xc = range(len(hist.epoch));
    f, (ax1, ax2) = plot.subplots(nrows=1, ncols=2);
    ax1.plot(xc, train_loss);
    ax1.plot(xc, val_loss)
    ax1.set_xlabel('num of Epochs');
    ax1.set_ylabel('loss');
    ax1.set_title('train_loss vs val_loss');
    ax1.grid(True);
    ax1.legend(['train', 'val']);
    
    ax2.plot(xc, train_acc);
    ax2.plot(xc, val_acc);
    ax2.set_xlabel('num of Epochs');
    ax2.set_ylabel('accuracy');
    ax2.set_title('train_acc vs val_acc');
    ax2.grid(True);
    ax2.legend(['train', 'val'], loc=4);
    f.show();


def main():
    # Print Model Parameters
    printParams();
    # Data Processing
    train = np.loadtxt(train_file_path, delimiter=",");
    test = np.loadtxt(test_file_path, delimiter=",");
    train_X = train[:, 0:64];
    train_Y = np_utils.to_categorical(train[:, 64]);
    test_X = test[:, 0:64];
    test_Y = np_utils.to_categorical(test[:, 64]);
    
    # NN Model
    model = Sequential();
    for i in range(nb_hidden_layers):
        if(i == 0):
            model.add(Dense(nb_hidden_units[0], activation=activation, input_dim=64));
            model.add(Dropout(dropout));
        model.add(Dense(nb_hidden_units[i], activation=activation));
        model.add(Dropout(dropout));
    # output layer
    model.add(Dense(10, activation="softmax"));
    
    # Training
    model.compile(optimizer=optimizer, loss=loss, metrics=metrics);
    hist = model.fit(x=train_X, y=train_Y, nb_epoch=nb_epoch, verbose=verbose, validation_split=validation_split,
                      callbacks=[early_stopping, CSVLogger("results/training.csv")]);
    # Evaluation
    train_scores = model.evaluate(train_X, train_Y, verbose=verbose);
    test_scores = model.evaluate(test_X, test_Y, verbose=verbose);
    y_train_pred = model.predict_classes(train_X, verbose=verbose);
    y_test_pred = model.predict_classes(test_X, verbose=verbose);
    print("\n Printing Training Metrics:", end="\n\n");
    printMetrics(train[:, 64], y_train_pred, train_scores);
    print("\n Printing Testing Metrics:", end="\n\n");
    printMetrics(test[:, 64], y_test_pred, test_scores);
    
    # Visualization
    if verbose:
        imPlot(hist);
        show();

if __name__ == '__main__':
    main();



    
